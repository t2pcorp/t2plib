# To generate token type H to request for Token Type C
## 1. Generate Type H live: 60secs
```
endpointURI = '/authen/v1/clientToken/generate'          << fix value
const clientKey = fs.readFileSync('client.key', 'utf8')  << read client key from secure storage file "client.key"  
body = '{"refernce":"any reference to type C if need"}'  << this text will use for reference only
var token = t2plib.generateTokenTypeH(body, clientKey, endpointURI)<< call lib function with above input

##The token output show as below, which is use as input for
  next step 2.
{
  header: 'ZXlKamJHbG......', << token Type H
  body: 'E94FXKbgTzpyR.....'  << the body will be encrypted
}
```


## 2. Request for Token type C live: 30mins (using above token output)
```
var request = require('request');
request.post({
           url: 'https://test-api-authen.t2p.co.th/authen/v1/clientToken/generate',
	       headers: {'Authorization': "Basic" +token.header},
           body: token.body
         }, function(error, response, body){
            console.log(body);
            // #if success the body response will contain "authToken" is type C
            //**
                {
                "meta": {
                    "language": "en_EN",
                    "responseCode": 1000,
                    "responseMessage": "Success",
                    "version": "1.0.0"
                },
                "data": {
                    "authToken": "ZXlKamJHbGxiblJEYjJSbElqb2lSbGR.............", << Token Type C
                    "dateOfExpire": "20230124232656",
                    "timeServer": "20230124225656"
                }
                }
            **//
         });
```
## 3. return this token type C to Mobile SDK for KYC onboarding
 --


#  To generate token type H to request to Wallet API
## 1. Prepare set endpointURI=''
```
endpointURI = ''
const clientKey = fs.readFileSync('client.key', 'utf8') 
data = '{"Example": "data to encrypt"}'

encryptedata = t2plib.encryptData(data, clientKey)
body = '{"data": "encryptedata"}'  

var token = t2plib.generateTokenTypeH(body, clientKey, endpointURI, false) << false to not encrypt body

##The token output show as below, which is use as input for API calls
{
  header: 'ZXlKamJHbG......', << token Type H
  body: '{"data": "encryptedata"}' 
}
```
## 2. Call APIs with token output
```
var request = require('request');
request.post({
           url: 'https://test-api-wallets.t2p.co.th/authen/v1/clientToken/testClientToken',
	       headers: {'Authorization': "Basic" +token.header},
           body: token.body
         }, function(error, response, body){
            console.log(body);
            // #if success the body response will looks like and "data" need to be decrypt
            //**
                {
                "meta": {
                    "language": "en_EN",
                    "responseCode": 1000,
                    "responseMessage": "Success",
                    "version": "1.0.0"
                },
                "data": "encryptedResponseData"
                }
            **//
         });
```
## 3. Decrypt Response Data
```
resp = JSON.parse(body)
const clientKey = fs.readFileSync('client.key', 'utf8') 
decryptedText = t2plib.decryptData(resp.data, clientKey)

// will get the plan text of data in decryptedText